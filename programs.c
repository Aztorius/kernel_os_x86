#include <programs.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <keyboard.h>
#include <interrupt.h>

void proc1(void)
{
    char command[64] = {0};

    while(true) {
        uint8_t com_size = 0;

        while (com_size == 0 || command[com_size-1] != '\n') {
            if (keyboard_get_next(command + com_size)) {
                printf("%c", command[com_size]);
                com_size++;
            }
        }

        command[com_size] = '\0';

        if (strncmp(command, "help", com_size - 1) == 0) {
            printf("help : print this message\n");
            printf("ps : list all processes\n");
            printf("killme : kill me\n");
        } else if (strncmp(command, "ps", com_size - 1) == 0) {
            print_processes();
        } else if (strncmp(command, "killme", com_size - 1) == 0) {
            printf("\e[31;40mGoodbye\e[39;49m\n");
            end_process();
        } else {
            printf("Unknown command\n");
        }

        printf(">");
    }
}

void proc2(void)
{
    /*printf("[%u s] priority %s process %s pid = %i\n",
        time_seconds(),
        process_priority(),
        process_name(),
        process_pid());*/
    process_sleep(1);
}

void proc3(void)
{
    for(;;) {
        /*printf("[%u s] priority %s process %s pid = %i\n",
            time_seconds(),
            process_priority(),
            process_name(),
            process_pid());*/
        process_sleep(5);
        create_process(&proc2, "DYNAMIC");
    }
}
