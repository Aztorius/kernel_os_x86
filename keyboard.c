#include <keyboard.h>
#include <cpu.h>
#include <stdio.h>
#include <string.h>

#define KEYMAP_LENGTH 64
#define BUFFER_LENGTH 32

static const char keymap[KEYMAP_LENGTH] = {
    27, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    'a', 'z', 'e', 'r', 't', 'y', 'u', 'i',
    'o', 'p', '^', '$', '\n', 0, 'q', 's',
    'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
    0, 0, 0, '*', 'w', 'x', 'c', 'v',
    'b', 'n', ',', ';', ':', '!', 0, 0,
    0, ' ', 0, 0, 0, 0, 0, 0
};

static char buffer[BUFFER_LENGTH] = {0};
static uint8_t buffer_size = 0;

void keyboard_interrupt(void)
{
    unsigned char key = inb(0x60);
    outb(0x20, 0x20);
    //printf("Keyboard : %u\n", key);
    if (key < KEYMAP_LENGTH && buffer_size < BUFFER_LENGTH) {
        buffer[buffer_size] = keymap[key];
        buffer_size++;
    }
}

bool keyboard_get_next(char *dest)
{
    if (buffer_size == 0) {
        return false;
    } else {
        buffer_size--;
        *dest = buffer[0];
        memmove(buffer, buffer + 1, buffer_size);
        return true;
    }
}
