#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <inttypes.h>
#include <stdbool.h>

extern void keyboard_interrupt(void);

extern bool keyboard_get_next(char *dest);

#endif
