#ifndef __INTERRUPT_H__
#define __INTERRUPT_H__

#include <inttypes.h>
#include <stdbool.h>

extern void init_traitant_IT(int32_t num_IT, void (*traitant)(void));

extern void masque_IRQ(uint32_t num_IRQ, bool masque);

extern void tic_PIT(void);

extern uint32_t time_seconds(void);

#endif
