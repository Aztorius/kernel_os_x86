#ifndef __PROCESS_H__
#define __PROCESS_H__

#include <inttypes.h>

#define PROCESS_MAX 16
#define PROCESS_NAME_LENGTH 16
#define PROCESS_CALLSTACK_LENGTH 512
#define PROCESS_REGS_LENGTH 5

enum process_state {
    LOW_PRIORITY,
    NORMAL_PRIORITY,
    HIGH_PRIORITY,
    SLEEPING,
    DYING,
    NB_PROCESS_STATE
};

struct process {
    int32_t pid;
    char name[PROCESS_NAME_LENGTH];
    enum process_state state;
    uint32_t wakeup_time;
    uint32_t regs[PROCESS_REGS_LENGTH];
    uint32_t *callstack;
    struct process *next;
};

extern void init_processes(void);

extern void scheduler(void);

extern int32_t create_process(void (*func)(void), char *name);

extern void process_sleep(uint32_t seconds);

extern void end_process(void);

extern void print_processes(void);

extern int32_t process_pid(void);

extern char *process_name(void);

extern char *process_priority(void);

#endif
