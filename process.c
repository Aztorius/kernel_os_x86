#include <process.h>
#include <stdio.h>
#include <cpu.h>
#include <malloc.c.h>
#include <interrupt.h>
#include <programs.h>

extern void ctx_sw(uint32_t*, uint32_t*);

static struct process *current_process = NULL;
static struct process *processes_head[NB_PROCESS_STATE] = { NULL };
static struct process *processes_tail[NB_PROCESS_STATE] = { NULL };

static int32_t current_max_pid = -1;

void insert_process(struct process *proc, enum process_state state)
{
    proc->state = state;
    if (processes_tail[state] == NULL) {
        processes_head[state] = proc;
        processes_tail[state] = proc;
    } else {
        processes_tail[state]->next = proc;
        processes_tail[state] = proc;
    }
}

struct process *extract_process(enum process_state state)
{
    if (processes_head[state] != NULL) {
        struct process *proc = processes_head[state];
        processes_head[state] = processes_head[state]->next;

        if (processes_head[state] == NULL) {
            processes_tail[state] = NULL;
        }

        proc->next = NULL;
        return proc;
    } else {
        return NULL;
    }
}

struct process *extract_next_process(void)
{
    if (processes_head[HIGH_PRIORITY] != NULL) {
        return extract_process(HIGH_PRIORITY);
    } else if (processes_head[NORMAL_PRIORITY] != NULL) {
        return extract_process(NORMAL_PRIORITY);
    } else {
        return extract_process(LOW_PRIORITY);
    }
}

/*
 * Try to wake up the process at the head of the sleeping chain if needed
 */
void handle_sleeping_processes(void)
{
    if (processes_head[SLEEPING] != NULL) {
        if (processes_head[SLEEPING]->wakeup_time <= time_seconds()) {
            insert_process(extract_process(SLEEPING), HIGH_PRIORITY);
        }
    }
}

void handle_dying_processes(void)
{
    if (processes_head[DYING] != NULL) {
        struct process *proc = processes_head[DYING];
        processes_head[DYING] = processes_head[DYING]->next;

        if (proc == processes_tail[DYING]) {
            processes_tail[DYING] = processes_head[DYING];
        }

        free(proc->callstack);
        free(proc);
    }
}

void end_process(void)
{
    struct process *dying_process = current_process;
    insert_process(dying_process, DYING);
    current_process = extract_next_process();
    ctx_sw(dying_process->regs, current_process->regs);
}

void process_sleep(uint32_t seconds)
{
    struct process *proc = current_process;
    proc->state = SLEEPING;
    proc->wakeup_time = seconds + time_seconds();

    if (processes_head[SLEEPING] == NULL) {
        processes_head[SLEEPING] = proc;
        processes_tail[SLEEPING] = proc;
        proc->next = NULL;
    } else {
        struct process *proc2 = processes_head[SLEEPING], *previous = proc2;
        while (proc2->wakeup_time < proc->wakeup_time) {
            previous = proc2;
            proc2 = proc2->next;

            if (proc2 == NULL) {
                break;
            }
        }

        if (previous != proc2) {
            previous->next = proc;
        }

        proc->next = proc2;

        if (proc2 == processes_head[SLEEPING]) {
            processes_head[SLEEPING] = proc;
        }
        if (proc2 == NULL) {
            processes_tail[SLEEPING] = proc;
        }
    }
    current_process = extract_next_process();
    ctx_sw(proc->regs, current_process->regs);
}

int32_t process_pid(void)
{
    return current_process->pid;
}

char *process_name(void)
{
    return current_process->name;
}

char *process_state(struct process *proc)
{
    switch(proc->state){
        case HIGH_PRIORITY:
            return "HIGH";
        case NORMAL_PRIORITY:
            return "NORMAL";
        case LOW_PRIORITY:
            return "LOW";
        case SLEEPING:
            return "SLEEPING";
        case DYING:
            return "DYING";
        default:
            return "OTHER";
    }
}

char *process_priority(void)
{
    return process_state(current_process);
}

void scheduler(void)
{
    cli();

    static struct process *previous_process;

    previous_process = current_process;

    if (previous_process->state == LOW_PRIORITY) {
        /* Low priority processes are unchanged */
        insert_process(previous_process, LOW_PRIORITY);
    } else {
        /* High and normal priority processes are set to normal */
        insert_process(previous_process, NORMAL_PRIORITY);
    }

    current_process = extract_next_process();

    handle_sleeping_processes();
    handle_dying_processes();

    sti();

    ctx_sw(previous_process->regs, current_process->regs);
}

void idle(void)
{
    for (;;) {
        sti();
        hlt();
        cli();
    }
}

int32_t create_process(void (*func)(void), char *name)
{
    if (current_max_pid >= PROCESS_MAX) {
        return -1;
    }

    current_max_pid++;

    struct process *proc;

    proc = malloc(sizeof(struct process));
    proc->callstack = calloc(
        sizeof(uint32_t),
        PROCESS_CALLSTACK_LENGTH);
    proc->pid = current_max_pid;
    strcpy(proc->name, name);
    proc->regs[1] = (uint32_t)&proc->callstack[PROCESS_CALLSTACK_LENGTH-2];
    proc->callstack[PROCESS_CALLSTACK_LENGTH-1] = (uint32_t)end_process;
    proc->callstack[PROCESS_CALLSTACK_LENGTH-2] = (uint32_t)func;

    insert_process(proc, NORMAL_PRIORITY);

    return current_max_pid;
}

void print_processes(void)
{
    printf("[%s] PID %i NAME %s\n",
        process_state(current_process),
        current_process->pid,
        current_process->name);
    for (uint8_t state = 0; state < NB_PROCESS_STATE; ++state) {
        if (processes_head[state] != NULL) {
            struct process *current = processes_head[state];
            while(current != NULL) {
                printf("[%s] PID %i NAME %s\n",
                    process_state(current),
                    current->pid,
                    current->name);
                current = current->next;
            }
        }
    }
}

void init_processes(void)
{
    create_process(&idle, "IDLE");
    current_process = extract_next_process();
    current_process->state = LOW_PRIORITY;

    create_process(&proc1, "PROC1");
    create_process(&proc2, "PROC2");
    create_process(&proc3, "PROC3");

    idle();
}
