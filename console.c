#include <console.h>
#include <inttypes.h>
#include <cpu.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define SCREEN_SIZE_X 80
#define SCREEN_SIZE_Y 25
#define SCREEN_SIZE_XY 2000
#define STYLE_STRING_LENGTH 8

static uint16_t cursor_column = 0;
static uint16_t cursor_line = 0;

static uint8_t current_style = 0x0F;
static uint8_t style_string_size = 0;
static char style_string[STYLE_STRING_LENGTH] = { 0 };

/*
 * Return a pointer wich corresponds to the given coordinates
 */
uint16_t *ptr_mem(uint32_t lig, uint32_t col)
{
    return (uint16_t*)(0xB8000 + ((lig * SCREEN_SIZE_X + col) << 1));
}

/*
 * Write a caracter on the screen
 */
void write_car(uint32_t lig, uint32_t col, char c)
{
    uint16_t *address = ptr_mem(lig, col);
    *address = (current_style << 8) | c;
}

/*
 * Move cursor position
 */
void move_cursor(uint32_t lig, uint32_t col)
{
    uint16_t val = col + lig * SCREEN_SIZE_X;
    cursor_column = col;
    cursor_line = lig;
    outb(0x0F, 0x3D4);
    outb(val & 0xFF, 0x3D5);
    outb(0x0E, 0x3D4);
    outb(val >> 8, 0x3D5);
}

/*
 * Clear the screen with space caracters
 */
void clear_screen(void)
{
    uint16_t *ptr = ptr_mem(0, 0);
    for(uint16_t i = 0; i < SCREEN_SIZE_XY; ++i, ++ptr) {
        *ptr = 0x0F20;
    }
    move_cursor(0, 0);
}

/*
 * Move up all te lines
 */
void move_up(void)
{
    for(uint16_t i = 1; i < SCREEN_SIZE_Y; ++i) {
        memmove(ptr_mem(i-1, 0), ptr_mem(i, 0), SCREEN_SIZE_X << 1);
    }
    for(uint16_t i = 0; i < SCREEN_SIZE_X; ++i) {
        write_car(SCREEN_SIZE_Y - 1, i, 0x20);
    }
}

uint8_t ainsi_to_vga_color(uint8_t color)
{
    switch (color) {
        case 30: /* Black */
            return 0;
        case 31: /* Red */
            return 4;
        case 32: /* Green */
            return 2;
        case 33: /* Yellow */
            return 14;
        case 34: /* Blue */
            return 1;
        case 35: /* Magenta */
            return 5;
        case 36: /* Cyan */
            return 3;
        case 37: /* White */
            return 15;
        default:
            return 0;
    }
}

void set_style(void)
{
    uint8_t foreground = strtol(&style_string[2], NULL, 10);
    uint8_t background = strtol(&style_string[5], NULL, 10);

    if (foreground == 39 && background == 49) {
        current_style = 0x0F;
        return;
    }

    foreground = ainsi_to_vga_color(foreground);
    background = ainsi_to_vga_color(background - 10);

    current_style = 0x7F & ((background << 4) | foreground);
}

/*
 * Print or do an action corresponding to c
 */
void treat_car(char c)
{
    if (c > 0x1F && c < 0x7F) {
        /* c is a caracter */
        if (c == 0x5B && style_string_size == 1) {
            style_string[style_string_size] = c;
            style_string_size++;
            return;
        } else if (c > 0x2F
            && c < 0x3A
            && style_string_size > 1
            && style_string_size < STYLE_STRING_LENGTH) {
            style_string[style_string_size] = c;
            style_string_size++;
            return;
        } else if (c == 0x3B && style_string_size == 4) {
            style_string[style_string_size] = c;
            style_string_size++;
            return;
        } else if (c == 0x6D && style_string_size == STYLE_STRING_LENGTH - 1){
            set_style();
            style_string_size = 0;
            return;
        }

        write_car(cursor_line, cursor_column, c);
        cursor_column++;
        if (cursor_column == SCREEN_SIZE_X) {
            cursor_column = 0;
            cursor_line++;
            if (cursor_line == SCREEN_SIZE_Y) {
                move_up();
                cursor_line--;
            }
        }
        move_cursor(cursor_line, cursor_column);
    } else {
        switch (c) {
            case 0x08:
                /* move cursor to the left */
                if (cursor_column > 0) {
                    move_cursor(cursor_line, cursor_column - 1);
                }
                break;
            case 0x09:
                /* move cursor to the next tabulation */
                move_cursor(cursor_line, (cursor_column & 0xF8) + 8);
                break;
            case 0x0A:
                /* move cursor to the next line, column 0 */
                if (cursor_line == SCREEN_SIZE_Y - 1) {
                    move_up();
                    cursor_line--;
                }
                move_cursor(cursor_line + 1, 0);
                break;
            case 0x0C:
                /* clear and reset cursor */
                clear_screen();
                move_cursor(0, 0);
                break;
            case 0x0D:
                /* carriage return */
                move_cursor(cursor_line, 0);
                break;
            case 0x1B:
                /* escape */
                style_string[0] = c;
                style_string_size = 1;
                break;
        }
    }
}

void console_putbytes(const char *s, int len)
{
    for(uint16_t i = 0; i < len; ++i) {
        treat_car(s[i]);
    }
}

void console_uptime(uint32_t uptime)
{
    uint8_t seconds = uptime % 60;
    uptime -= seconds;
    uint8_t minutes = (uptime / 60) % 60;
    uptime -= 60 * minutes;
    uint8_t hours = uptime % 3600;

    uint8_t save_style = current_style;
    current_style = 0x70;

    char s[9];
    sprintf(s, "%02d:%02d:%02d", hours, minutes, seconds);
    for (uint16_t i = 0; i < 8; ++i) {
        write_car(0, SCREEN_SIZE_X - 8 + i, s[i]);
    }

    current_style = save_style;
}
