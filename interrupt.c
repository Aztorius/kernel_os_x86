#include <interrupt.h>
#include <cpu.h>
#include <console.h>
#include <segment.h>
#include <process.h>

static uint32_t uptime = 0;
static uint8_t uptime_tics = 0;

uint32_t time_seconds(void)
{
    return uptime;
}

void init_traitant_IT(int32_t num_IT, void (*traitant)(void))
{
    uint32_t *ivt_it = (uint32_t *)0x1000 + num_IT * 2;
    *ivt_it = (KERNEL_CS << 16) | ((uint32_t)(traitant) & 0xFFFF);
    *(ivt_it + 1) = ((uint32_t)(traitant) & 0xFFFF0000) | 0x8E00;
}

void masque_IRQ(uint32_t num_IRQ, bool masque)
{
    unsigned char irq = inb(0x21);
    outb((irq & (masque << num_IRQ)) | (masque << num_IRQ), 0x21);
}

void tic_PIT(void)
{
    outb(0x20, 0x20);
    uptime_tics++;
    if (uptime_tics >= 50) {
        uptime_tics = 0;
        uptime++;
        console_uptime(uptime);
    }
    scheduler();
}
