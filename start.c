#include <cpu.h>
#include <inttypes.h>
#include <stdio.h>
#include <segment.h>
#include <interrupt.h>
#include <process.h>

#define QUARTZ 0x1234DD
#define CLOCKFREQ 50

extern void traitant_IT_32(void);
extern void traitant_IT_33(void);

void kernel_start(void)
{
    // init IVT for IT 32 and IT 33
    init_traitant_IT(32, &traitant_IT_32);
    init_traitant_IT(33, &traitant_IT_33);

    printf("\fGladOS\n>");

    // set clock frequency
    outb(0x34, 0x43);
    outb((QUARTZ / CLOCKFREQ) % 256, 0x40);
    outb(((QUARTZ / CLOCKFREQ) >> 8) & 0xFF, 0x40);

    // démasquage de l'IRQ 0 (horloge programmable)
    masque_IRQ(0, false);
    // démasquage de l'IRQ 1 (keyboard)
    masque_IRQ(1, false);

    init_processes();

    // on ne doit jamais sortir de kernel_start
    while (1) {
        // cette fonction arrete le processeur
        hlt();
    }
}
