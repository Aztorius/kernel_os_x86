#!/bin/bash

mkdir -p isofiles/boot/grub

cat <<EOM > isofiles/boot/grub/grub.cfg
set timeout=0
set default=0

menuentry "GladOS" {
    multiboot2 /boot/kernel.bin
    boot
}
EOM

make kernel.bin

cp kernel.bin isofiles/boot/

grub2-mkrescue -o GladOS.iso isofiles
